# The Repl Interface for Clem, the Clojure Error Mediator

Clem is a system for improving Clojure error messages, designed with beginners
in mind. Users can opt-in to Clem at a REPL. When an error occurs, Clem
intercepts the error, sends it to a web app, and gets a friendly explanation
back, assuming there is one, which it then shows to the user in place of the
default one. More experienced Clojure users can use the web app to create and
improve the friendly messages that are returned.

For more information on the web app portion, since the [Clem project
repo](https://gitlab.com/unc-app-lab/clem).

## Quick start

If you aren't in a project context but want to try Clem out, you can start a
one-off REPL with Clem enabled. How you do that depends on whether you're
running the Clojure CLI or Leiningen.

| Prerequisites | Command |
|-|-|
| [The Clojure CLI](https://clojure.org/guides/getting_started) | `clj -Sdeps '{:deps {clem-repl/clem-repl {:mvn/version "0.1.6"}}}'` |
| [Leiningen](https://leiningen.org/), [the lein-try plugin](https://github.com/avescodes/lein-try) | `lein try clem-repl 0.1.6` |

If on windows you may need to double escape quotes (so use `""0.1.6""`
for the version string)

After starting a REPL with Clem, start the Clem error interceptor with:

```
(require '[edu.unc.applab.clem-repl.core :as clem-repl])
(clem-repl/start!)
```

## Installation

You can also install Clem as a part of your project. How you do that depends on
which tool you're using to resolve dependencies.

For `deps.edn` include the following dependency:
```
{clem-repl {:mvn/version "0.1.6"}}
```

For Leiningen/Boot:
```
[clem-repl "0.1.6"]
```

## Usage

There are two ways to start Clem: manually in a REPL or via nREPL
configuration.

### REPL usage

In a REPL, run:

```
(require '[edu.unc.applab.clem-repl.core :as clem-repl])
(clem-repl/start!)
```

Any exceptions that occur will be intercepted by Clem.

### nREPL Middleware

If you're using an nREPL connection (as is default for Lein or Boot), you can
instead add `edu.unc.applab.clem-repl.nrepl-middleware/clem-caught` to your
nREPL middleware. The most general way to do this is by adding it to the
`:middleware` key in the `.nrepl-edn` file in your project home directory (the
directory containing `project.clj`, `build.boot`, or `deps.edn`). For example:

```
{:middleware [edu.unc.applab.clem-repl.nrepl-middleware/clem-caught]}
```

If you use the middleware, you do not need to run the `start!` function; Clem
will be enabled automatically.

Clem does not directly depend on `nrepl`, as some projects may not use it, but
is currently compiled against version "0.7.0-alpha3". This will probably be
automatically included by your build tool.

## Configuration

Clem will prompt for required configuration when started and has sensible
defaults for project-level configuration, so most users will simply accept
those. But if you want to add user level configuration, or change configuration
after initial setup, that is detailed here.

Project configuration is stored in a  file named `.clem-repl.edn` in the
project home directory (the same directory as `project.clj`, `boot.build`, or
`deps.edn`).

The global config file is located in
`%USERPROFILE%\\AppData\\Local\\clem-repl-global.edn` for Windows,
`~/Library/Application Support/clem-repl/clem-repl-global.edn` for Mac, and
`$XDG_CONFIG_HOME/clem-repl-global.edn` for Linux (defaults to `~/.config`).

An example barebones config file is as follows:

```
{:host-domain "http://clem.applab.unc.edu"
 :project-namespace 'my-com.my-proj}
```

### Required Configuration

While required, Clem will automatically prompt for this on start up.

*Clem Host Domain*

`:host-domain` should contain a URL to a running Clem instance. This is the
server that contains the explanation messages for exceptions.
`"http://clem.applab.unc.edu"` is the URL for the default server provided by
the AppLab. If you have another server to use replace
"http://clem.applab.unc.edu" with its URL. For example, a server based in a
different language could be used. As of now we don't know of any of those.

*Project Namespace*

`:project-namespace` is the namespace prefix for your project. Clem uses it to
find your code in an exception stacktrace and insert it into error
explanations. (This is still in alpha, and your mileage may vary.) It should be
the longest match prefix. For example, if you have three namespaces in your
project, `my-com.my-proj.core`, `my-com.my-proj.config`, and
`my-com.my-proj.db.conn`, then the prefix is `my-com.my-proj` because every
namespace begins with that.

### Optional Configuration

Clem has sensible defaults for these, and so only need to be entered into a
config file if there is a reason to change these defaults.

*Log File*

`:log-file` is the location for the clem log file, used for error reporting. It
defaults to `clem.log` in the project directory

## Running Tests

To run the unit tests once, run:

```
lein spec
```

To start a test runner, which will run tests anytime a source file changes, run:
```
lein spec -a
```
