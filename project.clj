(defproject clem-repl "0.1.6"
  :description "Repl Integration for Clem, the Clojure Error Mediator"
  :url "https://gitlab.com/unc-app-lab/clem-repl"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [clj-http "3.10.0"]
                 [environ "1.1.0"]
                 [clj-time "0.15.2"]]
  :profiles {:dev {:dependencies [[speclj "3.3.0"]
                                  [nrepl "0.7.0-alpha3"]]
                   :repl-options {:nrepl-middleware [edu.unc.applab.clem-repl.nrepl-middleware/clem-caught]}}}
  :plugins [[speclj "3.3.0"]]
  :source-paths ["src"]
  :test-paths ["spec"])
