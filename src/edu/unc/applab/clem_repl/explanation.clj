(ns edu.unc.applab.clem-repl.explanation
  (:require [clojure.java.io :as io]
            [clojure.string :as s]))

(defn- base-nms [nms]
  (let [dIndex (.indexOf nms "$")]
    (if (= dIndex -1)
      nms
      (.substring nms 0 dIndex))))

(defn- create-file-name [clazz]
  (println clazz)
  (-> (base-nms clazz)
      (.replace "." "/")
      (.replace "-" "_")
      (str ".clj")))

(defn fill
  "Returns the result of `(format explanation line)` where line is the
  line of code which the first stacktrace element in exception's stacktrace points to,
  where the class of the stack trace element starts with `nms`.

  If such a namespace is not found or the exception is nil, then it returns the result of
  `(format explanation \"\")`"
  [explanation exception nms]
  (let [nms (str nms)
        stk (first (filter
                    #(.startsWith (base-nms (:class %)) nms)
                    (:stack-trace exception)))]
    (format explanation
            (if stk
              (-> (create-file-name (:class stk))
                  io/resource
                  slurp
                  (s/split #"\n")
                  (nth (dec (:line stk))))
              ""))))
