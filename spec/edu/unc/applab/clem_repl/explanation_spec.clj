(ns edu.unc.applab.clem-repl.explanation-spec
  (:require [clojure.java.io :as io]
            [clojure.string :as s]
            [edu.unc.applab.clem-repl.explanation :as sut] ; Subject Under Test
            [speclj.core :refer :all]))

(def exception
  {:clojure-version "1.9.0",
   :class "class java.lang.Exception",
   :message "c",
   :timestamp 89,
   :stack-trace
   [{:class "edu.unc.applab.clem-repl.explanation$fill",
     :method "invokeStatic",
     :file "explanation.clj",
     :line 16}
    {:class "edu.unc.applab.clem-repl.explanation$fill",
     :method "invoke",
     :file "explanation.clj",
     :line 16}
    {:class "edu.unc.applab.clem-repl.main$eval13572",
     :method "invokeStatic",
     :file "form-init5719734133047754467.clj",
     :line 12}
    {:class "edu.unc.applab.clem-repl.main$eval13572",
     :method "invoke",
     :file "form-init5719734133047754467.clj",
     :line 12}
    {:class "clojure.lang.Compiler",
     :method "eval",
     :file "Compiler.java",
     :line 7062}
    {:class "clojure.lang.Compiler",
     :method "eval",
     :file "Compiler.java",
     :line 7025}
    {:class "clojure.core$eval",
     :method "invokeStatic",
     :file "core.clj",
     :line 3206}
    {:class "clojure.core$eval",
     :method "invoke",
     :file "core.clj",
     :line 3202}
    {:class "clojure.main$repl$read_eval_print__8572$fn__8575",
     :method "invoke",
     :file "main.clj",
     :line 243}
    {:class "clojure.main$repl$read_eval_print__8572",
     :method "invoke",
     :file "main.clj",
     :line 243}
    {:class "clojure.main$repl$fn__8581",
     :method "invoke",
     :file "main.clj",
     :line 261}
    {:class "clojure.main$repl",
     :method "invokeStatic",
     :file "main.clj",
     :line 261}
    {:class "clojure.main$repl",
     :method "doInvoke",
     :file "main.clj",
     :line 177}
    {:class "clojure.lang.RestFn",
     :method "applyTo",
     :file "RestFn.java",
     :line 137}
    {:class "clojure.core$apply",
     :method "invokeStatic",
     :file "core.clj",
     :line 657}
    {:class "clojure.core$apply",
     :method "invoke",
     :file "core.clj",
     :line 652}
    {:class
     "refactor_nrepl.ns.slam.hound.regrow$wrap_clojure_repl$fn__10363",
     :method "doInvoke",
     :file "regrow.clj",
     :line 18}
    {:class "clojure.lang.RestFn",
     :method "invoke",
     :file "RestFn.java",
     :line 1523}
    {:class
     "clojure.tools.nrepl.middleware.interruptible_eval$evaluate$fn__872",
     :method "invoke",
     :file "interruptible_eval.clj",
     :line 87}
    {:class "clojure.lang.AFn",
     :method "applyToHelper",
     :file "AFn.java",
     :line 152}
    {:class "clojure.lang.AFn",
     :method "applyTo",
     :file "AFn.java",
     :line 144}
    {:class "clojure.core$apply",
     :method "invokeStatic",
     :file "core.clj",
     :line 657}
    {:class "clojure.core$with_bindings_STAR_",
     :method "invokeStatic",
     :file "core.clj",
     :line 1965}
    {:class "clojure.core$with_bindings_STAR_",
     :method "doInvoke",
     :file "core.clj",
     :line 1965}
    {:class "clojure.lang.RestFn",
     :method "invoke",
     :file "RestFn.java",
     :line 425}
    {:class "clojure.tools.nrepl.middleware.interruptible_eval$evaluate",
     :method "invokeStatic",
     :file "interruptible_eval.clj",
     :line 85}
    {:class "clojure.tools.nrepl.middleware.interruptible_eval$evaluate",
     :method "invoke",
     :file "interruptible_eval.clj",
     :line 55}
    {:class
     "clojure.tools.nrepl.middleware.interruptible_eval$interruptible_eval$fn__917$fn__920",
     :method "invoke",
     :file "interruptible_eval.clj",
     :line 222}
    {:class
     "clojure.tools.nrepl.middleware.interruptible_eval$run_next$fn__912",
     :method "invoke",
     :file "interruptible_eval.clj",
     :line 190}
    {:class "clojure.lang.AFn",
     :method "run",
     :file "AFn.java",
     :line 22}
    {:class "java.util.concurrent.ThreadPoolExecutor",
     :method "runWorker",
     :file "ThreadPoolExecutor.java",
     :line 1149}
    {:class "java.util.concurrent.ThreadPoolExecutor$Worker",
     :method "run",
     :file "ThreadPoolExecutor.java",
     :line 624}
    {:class "java.lang.Thread",
     :method "run",
     :file "Thread.java",
     :line 748}],
   :message-explanation nil,
   :caused-by nil})

(defn- get-line-of-file [line file]
  (-> (io/resource file)
      slurp
      (s/split #"\n")
      (nth (dec line))))

(describe "Fill"

  (it "fills in the correct line into the explanation"
    (should= (str "Code line: " (get-line-of-file 16 "edu/unc/applab/clem_repl/explanation.clj"))
             (sut/fill "Code line: %s" exception 'edu.unc.applab.clem-repl)))

  (it "simply replaces with an empty string if the file is not found"
    (should= "Code line: " (sut/fill "Code line: %s" exception 'not.here)))

  (it "simply replaces with an empty string if exception is nil"
    (should= "Code line: " (sut/fill "Code line: %s" nil 'edu.unc.applab.clem-repl))))
