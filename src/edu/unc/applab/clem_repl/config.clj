(ns edu.unc.applab.clem-repl.config
  (:require [clojure.edn :as edn]
            [clojure.java.io :as io]
            [clojure.pprint :as pprint]
            [clojure.spec.alpha :as s]
            [clojure.string :as string]
            [environ.core :refer [env]])
  (:import java.net.MalformedURLException))

(defn uri-string? [x]
  (when x
    (try
      (io/as-url x)
      true
      (catch MalformedURLException e
        false))))

(s/def ::host-domain uri-string?)
(s/def ::project-namespace symbol?)
(s/def ::log-file string?)
(s/def ::disable-clem-logging boolean?)

(s/def ::config-part (s/keys :opt-un [::host-domain ::project-namespace
                                      ::log-file ::disable-clem-logging]))
(s/def ::config-final (s/keys :req-un [::host-domain ::project-namespace]
                              :opt-un [::log-file ::disable-clem-logging]))

(def user-config "clem-repl-global.edn")
(def project-config ".clem-repl.edn")

(defn- find-user-config-file []
  (io/file (let [os (.toLowerCase (System/getProperty "os.name"))]
             (if (.contains os "windows")
               (str (:userprofile env) "\\AppData\\Local")
               (if (.contains os "mac os x")
                 (io/file (:home env) "Library/Application Support/clem-repl/")
                 ;; Assuming it's Linux if it's not Windows or Mac
                 ;; Other OSes not supported
                 (or
                  (:xdg-config-home env)
                  (io/file (:home env) ".config")))))
           user-config))

(defn- get-user-config []
  (let [user-config-file (find-user-config-file)]
    (if (and user-config-file (.exists user-config-file))
      (edn/read-string (slurp user-config-file))
      {})))

(defn- find-project-config-file
  ([current-dir]
   (if (some identity (map
                       #(.exists (io/file current-dir %))
                       ["build.boot" "project.clj" "deps.edn"]))
     (io/file current-dir project-config)
     (if-let [parentFile (.getParentFile current-dir)]
       ;; The parentFile of the root folder is nil
       (find-project-config-file parentFile)
       nil)))
  ([]
   (find-project-config-file (.getParentFile (io/file (.getAbsolutePath (io/file ".")))))))

(defn- get-project-config []
  (let [config-file (find-project-config-file)]
    (if (and config-file (.exists config-file))
      (edn/read-string (slurp config-file))
      {})))

(defn- get-possible-keys []
  (map #(-> % name keyword)
       (let [desc (s/describe ::config-part)]
         (loop [previous nil
                current (first desc)
                next (rest desc)]
           (if (= previous :opt-un)
             current
             (recur current (first next) (rest next)))))))

(defn- get-env-config []
  (reduce (fn [result [k v]]
            (let [k-str (str k)
                  prefix ":clem-"]
              (if (and (string/starts-with? k-str prefix)
                       (> (count k-str) (count prefix)))
                (let [env-key (keyword (subs k-str (count prefix)))]
                  (if (some #{env-key} (get-possible-keys))
                    (assoc result env-key v)
                    result))
                result)))
          {}
          env))

(defn save-config!
  "Saves v as the value to key k in the config.

  Defaults to saving in the project-config, but if :user is passed as scope, then
  it will save to the user config."
  ([k v scope]
   (let [config (if (= :user scope)
                  (get-user-config)
                  (get-project-config))
         output-file (if (= :user scope)
                       (find-user-config-file)
                       (find-project-config-file))]
     (spit output-file (pr-str (assoc config k v)))))
  ([k v]
   (save-config! k v :project)))

(defn prompt-return!
  "Prompts for a value for a passed config, transforms it, saves it to the result, and
  adds it to the config. Returns the config.

  `config` is the passed in config. Treats `k` as a spec and checks if the config
  contains a valid value at `k`.  If it is not, the user is prompted for it, using
  `prompt` with `default` as a default value. `transform-func` is applied to the value
  before it is saved (this defaults to identity, essentially meaning using a string).

  `transform-func` will not be applied to default"
  ([config k prompt default transform-func]
   (let [config-key (keyword (name k))]
     (if (not (s/valid? k (get config config-key)))
       (do
         (println "Missing CLEM config option. Please see https://gitlab.com/unc-app-lab/clem-repl/#Required-Configuration for info on options.")
         (print prompt (str "[" default "]: "))
         (flush)
         (let [result (read-line)
               scope (if (find-project-config-file) :project :user)
               to-save (if (= "" (.trim result))
                         default
                         (transform-func result))]
           (if (s/valid? k to-save)
             (do
               (save-config! config-key to-save scope)
               (assoc config
                      config-key
                      to-save))
             (do
               (println "Error! Invalid required config key, please check for misspellings and try again.")
               (recur config k prompt default transform-func)))))
       config)))
  ([config k prompt default]
   (prompt-return! config k prompt default identity)))

(defn prompt-missing-config!
  [config]
  (-> config
      (prompt-return! ::host-domain "Clem Host Domain" "http://clem.applab.unc.edu")
      (prompt-return! ::project-namespace "Project Namespace" (symbol (str *ns*)) symbol)))


(defn load-config
  "Returns a map of config objects as defined in four locations: a user-wide config,
  a project config, enviroment variables, and an optional overrides map passed to
  the function. Each source overrides the previous ones.

  The global config file located in %USERPROFILE%\\AppData\\Local\\clem-repl-global.edn
  for Windows, ~/Library/Application Support/clem-repl/clem-repl-global.edn for Mac, and
  $XDG_CONFIG_HOME/clem-repl-global.edn for Linux (defaults to ~/.config).

  The project config is found in closest directory to the runtime directory with a
  build.boot, a project.clj, or a deps.edn file in a file named .clem-repl.edn.

  If the compiled config is invalid, it returns a string explaining why it is invalid."
  ([]
   (load-config {}))
  ([overrides]
   (let [user-config (get-user-config)
         project-config (get-project-config)
         env-config (get-env-config)
         final-result (merge
                       user-config
                       project-config
                       env-config
                       overrides)]
     (if (s/valid? ::config-final final-result)
       final-result
       (do (println "Warning: clem config is invalid. Clem may try to prompt for changes.")
           final-result)))))
