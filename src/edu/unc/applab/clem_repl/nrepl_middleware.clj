(ns edu.unc.applab.clem-repl.nrepl-middleware
  (:require [edu.unc.applab.clem-repl.config :as config]
            [edu.unc.applab.clem-repl.core :as core]
            [nrepl.middleware :as m]
            [nrepl.transport :as t]))

(defn clem-caught
  "nREPL middleware that activates the clem-repl-caught function"
  [handler]
  (fn
    [msg]
    (handler (if (= "eval" (:op msg))
               (assoc msg
                      :nrepl.middleware.caught/caught 'edu.unc.applab.clem-repl.core/clem-repl-caught
                      :nrepl.middleware.caught/print? true)
               msg))))

(m/set-descriptor!
 #'clem-caught
 {:expects #{#'nrepl.middleware.caught/wrap-caught}})
