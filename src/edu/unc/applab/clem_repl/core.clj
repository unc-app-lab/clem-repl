(ns edu.unc.applab.clem-repl.core
  (:require clojure.main
            [clj-http.client :as http]
            clj-time.coerce
            clj-time.core
            [clojure.spec.alpha :as s]
            [clojure.java.io :as io]
            [edu.unc.applab.clem-repl.explanation :as expl]
            [edu.unc.applab.clem-repl.config :as config]
            [taoensso.timbre :as timbre]
            [taoensso.timbre.appenders.core :as appenders]))

(defn- stack-trace-element->map [element]
  {:class (.getClassName element)
   :method (.getMethodName element)
   :file (.getFileName element)
   :line (.getLineNumber element)})

(defn error->map
  "Converts an exception object to a map matching the spec
  :edu.unc.applab.clem.spec/exception and specifies its timestamp as the passed value."
  [error timestamp]
  (when error
    {:clojure-version (clojure-version)
     :class (str (.getClass error))
     :phase (-> error
                Throwable->map
                clojure.main/ex-triage
                :clojure.error/phase)
     :message (.getMessage error)
     :timestamp timestamp
     :stack-trace (mapv stack-trace-element->map (.getStackTrace error))
     :message-explanation nil
     :caused-by (error->map (.getCause error) timestamp)}))

(defn clem-failed
  [e]
  (println "Clem had an error getting friendly error messages. Here is the ordinary error message.")
  (clojure.main/repl-caught e))

(defn generate-log-config [log-file]
  {:level :debug
   ;;:ns-whitelist ["edu.unc.applab.clem-repl"]
   :timestamp-opts timbre/default-timestamp-opts
   :output-fn timbre/default-output-fn
   :appenders {:spit (appenders/spit-appender {:fname (or log-file "clem.log")})}})

(defn clem-repl-caught
  "Takes the passed exception and calls the /error endpoint at the passed domain name
  in order to receive a found-exception. If that exception has an explanation then it
  prints that explanation with the user's data and the stacktrace. Otherwise it calls
  clojure.main/repl-caught.

  nms represents the namespace the user's code is in, and is used to find the first file
  with the user's code in order to fill out the friendly error message."
  ([log-config nms domain e]
   (binding [*out* *err*]
     (try
       (let [exception-map (error->map e (clj-time.coerce/to-long (clj-time.core/now)))
             resp (http/put
                   (str domain "/api/error")
                   {:throw-exceptions false
                    :body (prn-str exception-map)
                    :content-type "application/edn"})
             {:keys [status body]} resp]
         (if (#{200 201} status)
           (let [ex-map (clojure.edn/read-string body)
                 message (:message-explanation ex-map)]
             (if message
               (do
                 (println (-> e class .getSimpleName) (.getMessage e))
                 (println (expl/fill message ex-map nms)))
               (clojure.main/repl-caught e))
             (println "View this error on CLEM:" (str domain "/#/error/" (:id ex-map))))
           (do
             (timbre/log* log-config :error "Invalid HTTP Response")
             (timbre/log* log-config :error resp)
             (clem-failed e))))
       (catch Exception clem-exception
         (timbre/log* log-config :error "Error access exception on CLEM")
         (timbre/log* log-config :error clem-exception)
         (clem-failed e)))))
  ([e]
   (let [{:keys [host-domain project-namespace log-file]} (config/prompt-missing-config! (config/load-config))]
     (clem-repl-caught (generate-log-config log-file) project-namespace host-domain e))))

(defn start! []
  (println "Starting the Clem sub-REPL...hit control-d to exit")
  (let [{:keys [host-domain project-namespace
                log-file]} (config/prompt-missing-config! (config/load-config))
        log-config (generate-log-config log-file)]
    ;; Log config is passed in because clem-repl is a client program, and we don't want
    ;; to mess with anyone's setup.
    (clojure.main/repl
     :caught (partial clem-repl-caught log-config project-namespace host-domain)))
  (println "Thanks for using Clem! Returning you to your original REPL now."))

(comment
  (require 'edu.unc.applab.clem-repl.core)
  (edu.unc.applab.clem-repl.core/start!)
  )
